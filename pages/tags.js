import React from 'react';
import {useEffect,useState} from 'react';
import { StyleSheet, Text, View,Button,TextInput} from 'react-native';
import { FontAwesome5 } from '@expo/vector-icons';
import Tag from '../components/Tag.js'
import NewTag from '../components/NewTag.js'
import colors from '../colors.js';
import guid from '../guid.js';
import AsyncStorage from '@react-native-async-storage/async-storage';


export default function Tags() {

	const [tags,setTags] = useState(
		[
		]
	)
	const add = (value) =>{
		const t = [...tags]
		t.push({id:guid(),name:value});
		setTags(t)
		save(t)
	}
	const save = async (tags) =>{
		try {
    		const jsonValue = JSON.stringify(tags)
    		await AsyncStorage.setItem('tags', jsonValue)
  		} catch (e) {
    		console.error(e)	
  		}

	}
	const load =  () => {
    	AsyncStorage.getItem('tags').then(jsonValue =>{
    		const t = jsonValue != null ? JSON.parse(jsonValue) : null;
    		if(t){
    			setTags(t)
    		}
    	})
  	}
	const remove = (id) =>{
		const t = tags.filter((elem) => elem.id !== id)
		setTags(t)
		save(t)
	}
	useEffect(load)
	return (
		<View>
			{tags.map((tag,key) => {
				return <Tag key={key} tag={tag} remove={remove}> </Tag>
			}
			)}
			<NewTag add={add}/>
		</View>
	)
}
