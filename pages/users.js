import React from 'react';
import {useEffect,useState} from 'react';
import { StyleSheet, Text, View,Button} from 'react-native';
import User from '../components/user.js'
import NewUser from '../components/NewUser.js'
import AsyncStorage from '@react-native-async-storage/async-storage';



export default function Users() {
	const [users,setUsers] = useState(
		[

		]
	)
	const add = (value) =>{
		const u = [...users]
		u.push(value);
		setUsers(u)
		save(u)
	}
	const save = async (users) =>{
		try {
    		const jsonValue = JSON.stringify(users)
    		await AsyncStorage.setItem('users', jsonValue)
  		} catch (e) {
    		console.error(e)	
  		}

	}
	const load =  () => {
    	AsyncStorage.getItem('users').then(jsonValue =>{
    		const t = jsonValue != null ? JSON.parse(jsonValue) : null;
    		if(t){
    			setUsers(t)
    		}
    	})
  	}
	const remove = (id) =>{
		const t = users.filter((elem) => elem.id !== id)
		setUsers(t)
		save(t)
	}
	useEffect(load)
	return (
		<View>
			{users.map((user,key) => {
				return <User key={key} remove={remove} userData={user}/>
			}
			)}

			<NewUser add={add}/>
		</View>
	)
}
