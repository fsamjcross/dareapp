import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import colors from '../colors.js';

const Header = ({title}) => {
  return (
    <View style={styles.header}>
      <Text style={styles.text}>{title}</Text>
    </View>
  );
};

Header.defaultProps = {
  title: 'page',
};

const styles = StyleSheet.create({
  header: {
    height: 60,
  	padding:10,
    backgroundColor: '#' + colors[1],
  },
  text: {
    color:'#' + colors[4],
    fontSize: 23,
    textAlign: 'center',
  },
});

export default Header;
