
import React from 'react';
import {View,Button, Text, StyleSheet, Modal,TextInput} from 'react-native';
import { FontAwesome5 } from '@expo/vector-icons';
import colors from '../colors.js';
import Header from './Header';

const styles = StyleSheet.create({
	fullscreen:{
		position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0
	},
	container:{
		flex: 1,
        flexDirection: 'column',
        justifyContent: 'space-between',	
        alignItems: 'center',
        padding:10,
	},
	add:{
		flex:1,
        backgroundColor: '#' + colors[2],
        color:'#' + colors[0],
        fontSize:10,
        alignSelf:'center'
	},
	footer: {
        height: 60,
      	flexDirection: 'row',
      	justifyContent: 'space-evenly',
		borderStyle:'solid',
		borderTopWidth:1,
      	paddingTop:15,
      	paddingBottom:10,
      	position:'absolute',
    	right:0,
    	left:0,
    	bottom:0,
        backgroundColor: '#' + colors[1],
    	color:'#' + colors[4]
    },
	TagsActrive:{
		borderStyle:'solid',
		borderBottomWidth:1,
		borderColor:'gray',
		padding:20,
		flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
	},
	title:{
		height: 60,
		borderColor: 'gray',
		borderWidth: 1,
		margin:10,
		padding:10
	},
	TitleHeader:{
		margin:10,
		fontSize:30
	}
})
const NewTag = ({add}) => {
  	const [value, onChangeText] = React.useState('');
  	const [isOpen, setisOpen] = React.useState(false);
  	const save = ()=>{
		add(value)
		onChangeText('')
  		setisOpen(false)
  	}
  	const remove = ()=>{
		onChangeText('')
  		setisOpen(false)
  	}
  	return(
  	  	<View>
  	  	  	<View style={styles.container}>
  	  	  	  	<View style={styles.container}>
			  	  	<FontAwesome5  name="plus" onPress={() => {setisOpen(true)}} size={48} color="black" />
	  	  	  	</View>
			{ isOpen &&
				<Modal style={styles.fullscreen}>
			  	  	<Header title='New Tag'></Header>
			  		<Text style={styles.TitleHeader}>Tag Name</Text>
	 				<TextInput
      					style={styles.title}
      					onChangeText={text => onChangeText(text)}
      					value={value}
    				/>		  	  	

					<View style={styles.footer}>
    					<FontAwesome5 name="trash" onPress={() => {remove()}}  size={24} color={'#' + colors[4]} />
    					<FontAwesome5 name="save" onPress={() => {save()}}  size={24} color={'#' + colors[4]} />
    				</View>

	  	  	  	</Modal>
				}
	  	  	</View>
	  	</View>
  	);
};

export default NewTag;
