
import React from 'react';
import {useEffect,useState} from 'react';
import {View,Button, Text, StyleSheet, Modal,TextInput} from 'react-native';
import { FontAwesome5 } from '@expo/vector-icons';
import colors from '../colors.js';
import Header from './Header';
import guid from '../guid'

import AsyncStorage from '@react-native-async-storage/async-storage';
const styles = StyleSheet.create({
	fullscreen:{
		position:'absolute',
		top:0,
		bottom:0,
		left:0,
		right:0
	},
	container:{
		flex: 1,
        flexDirection: 'column',
        justifyContent: 'space-between',	
        alignItems: 'center',
        padding:10,
	},
	add:{
		flex:1,
        backgroundColor: '#' + colors[2],
        color:'#' + colors[0],
        fontSize:10,
        alignSelf:'center'
	},
	footer: {
        height: 60,
      	flexDirection: 'row',
      	justifyContent: 'space-evenly',
		borderStyle:'solid',
		borderTopWidth:1,
      	paddingTop:15,
      	paddingBottom:10,
      	position:'absolute',
    	right:0,
    	left:0,
    	bottom:0,
        backgroundColor: '#' + colors[1],
    	color:'#' + colors[4]
    },
	TagsActrive:{
		borderStyle:'solid',
		borderBottomWidth:1,
		borderColor:'gray',
		padding:20,
		flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
	},
	title:{
		height: 60,
		borderColor: 'gray',
		borderWidth: 1,
		margin:10,
		padding:10
	},
	TitleHeader:{
		margin:10,
		fontSize:30
	},
	tag:{
		flexDirection: 'row',
        justifyContent: 'flex-start',
        paddingLeft:11,
        padding:11
	},
	checkbox:{
		flex:1,
		fontSize:20
	},
	TagText:{
		flex:1,
		fontSize:20
	}
})
const NewTag = ({add}) => {
  	const [user, setuser] = React.useState({});
  	const [isOpen, setisOpen] = React.useState(false);
  	const [tags,setTags] = useState(
		[
		]
	)
	const onChangeText = (name)=>{
		const u = {...user}	
		u.id = u.id || guid()
		u.name = name
		setuser(u)
	}
  	const save = ()=>{
		add(user)
		setuser({})
  		setisOpen(false)
  	}
  	const remove = ()=>{
		setuser({})
  		setisOpen(false)
  	}
	const loadTags =  () => {
    	AsyncStorage.getItem('tags').then(jsonValue =>{
    		const t = jsonValue != null ? JSON.parse(jsonValue) : null;
    		if(t){
    			setTags(t)
    		}
    	})
  	}
  	
	const ToggleTag = (id)=>{
		if(!id) return 
		const u = {...user}
  		u.tags = u.tags || {} 
  		u.tags[id] =  !u.tags[id]
		u.id = u.id || guid()
		setuser(u)
  	}
  	useEffect(loadTags)
  	const tagsDom = tags.map((tag,key)=>{
  		return (
  		<View key={key} style={styles.tag} >
    		{!user.tags?.[tag.id] && (<FontAwesome5 name="times-circle" onPress={() => {ToggleTag(tag.id)}}  style={styles.checkbox}  size={24} color='black' />)}
    		{user.tags?.[tag.id] && (<FontAwesome5 name="check-circle" onPress={() => {ToggleTag(tag.id)}}  style={styles.checkbox}  size={24} color='black' />)}
    		<Text onPress={() => {ToggleTag(tag.id)}}  style={styles.TagText} >{tag?.name}</Text>
		</View>)

  	})
  	return(
  	  	<View>
  	  	  	<View style={styles.container}>
  	  	  	  	<View style={styles.container}>
			  	  	<FontAwesome5  name="plus" onPress={() => {setisOpen(true)}} size={48} color="black" />
	  	  	  	</View>
			{ isOpen &&
				<Modal style={styles.fullscreen}>
			  	  	<Header title='User Name'></Header>
			  		<Text style={styles.TitleHeader}>User Name</Text>
	 				<TextInput
      					style={styles.title}
      					onChangeText={text => onChangeText(text)}
      					value={user.name}
    				/>		  	  	
					{tagsDom}	
					<Text>{JSON.stringify(user)}</Text>
					<View style={styles.footer}>
    					<FontAwesome5 name="trash" onPress={() => {remove()}}  size={24} color={'#' + colors[4]} />
    					<FontAwesome5 name="save" onPress={() => {save()}}  size={24} color={'#' + colors[4]} />
    				</View>

	  	  	  	</Modal>
				}
	  	  	</View>
	  	</View>
  	);
};

export default NewTag;
