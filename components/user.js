import React from 'react';
import { StyleSheet, Text, View,Button,Input} from 'react-native';
import { FontAwesome5 } from '@expo/vector-icons';
import Tag from './Tag.js'
import {useEffect,useState} from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';

const styles = StyleSheet.create({
	container:{
		padding:20,
		flex: 1,
    	flexDirection: 'row',
      	justifyContent: 'space-between',
	},
	child:{
		flex:1,
	},
	nameContainer:{
		flex:2,
	},
	name:{
    	fontSize: 40,
	},
	actions:{
		flex:0.5,
	},
	actionsIcon:{
		flex:1,
		paddingTop:20,
	},

	iconContaner:{
		justifyContent: 'center',
    	alignItems: 'center'
	},
	icon:{
		fontSize:100,
		flex:1
			},
	TagsContainer:{
    	flexDirection: 'column',
      	flexWrap: 'wrap',
	},
	Tags:{
		flex:1,
		margin:5,
  		borderRadius: 25,
		backgroundColor:'red',
		color:'black'
	},
	TagsActrive:{
	},
	title:{
	}
});

export default function User({userData,remove}) {

	const [tags,setTags] = useState(
		[
		]
	)
	const loadTags =  () => {
    	AsyncStorage.getItem('tags').then(jsonValue =>{
    		const t = jsonValue != null ? JSON.parse(jsonValue) : null;
    		if(t){
    			setTags(t)
    		}
    	})
  	}

	let tagsDom	= <Text style={styles.title}>This user has no tags</Text>
 
	if(userData.tags){
		tagsDom = Object.keys(userData.tags).map( (id,key)=>{
			if(!userData.tags[id]) return;
			tag = tags.filter((el)=> {
				return el.id == id
			})[0]
			if(!tag) return 
			return (<View key={key} style={styles.TagsActrive} > 
				<Text style={styles.title}>{tag?.name}</Text>
			</View>)
		})
	}
	
	useEffect(loadTags)
	return( 
		<View style={styles.container}>
			<View style={styles.child}>
				<FontAwesome5 style={styles.icon} name="user" size={24} color="black" />
			</View>
			<View style={styles.nameContainer}>
				<Text style={styles.name}>{userData.name}</Text>
				<View style={styles.TagsContainer}>
					{tagsDom}
				</View>
			</View>
			<View style={styles.actions}>
				<FontAwesome5 style={styles.actionsIcon} name="edit" size={24} color="black" />
				<FontAwesome5 style={styles.actionsIcon} onPress={()=>{remove(userData.id)}}name="trash" size={24} color="black" />
			</View>

		</View>

	)
}
