import React from 'react';
import {useState} from 'react';

import { StyleSheet, Text, View,Button} from 'react-native';
import {primery,tertiary} from '../colors.js';
import { FontAwesome5 } from '@expo/vector-icons';

const styles = StyleSheet.create({
	TagsActrive:{
		borderStyle:'solid',
		borderBottomWidth:1,
		borderColor:'gray',
		padding:20,
		flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
	},
	title:{
		fontSize:30,
	},
	trash:{
	}
})
export default function Tags({tag,remove}) {
	return(	
		<View style={styles.TagsActrive} > 
			<Text style={styles.title}>{tag?.name}</Text>
			{remove && <FontAwesome5 onPress={()=> remove(tag?.id)} style={styles.trash} name="trash" size={24} color="black" />}
		</View>
	)
}

