import { StatusBar } from 'expo-status-bar';
import React from 'react';
import {useState} from 'react';
import { ScrollView ,StyleSheet, Text, View,Button} from 'react-native';
import { FontAwesome5 } from '@expo/vector-icons';
import UsersPage from './pages/users.js';
import TagsPage from './pages/tags.js';
import colors from './colors.js';
import Header from './components/Header';


const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop:30,
        backgroundColor: '#' + colors[1],
    },
    inputWrapper: {
		
    },
    contentContainer: {
    	position:'absolute',
    	right:0,
    	left:0,
    	top:90,
    	bottom:60,
    	overflow:'scroll',
        backgroundColor: '#' + colors[4],
    },
    footer: {
        height: 60,
      	flexDirection: 'row',
      	justifyContent: 'space-evenly',
		borderStyle:'solid',
		borderTopWidth:1,
      	paddingTop:15,
      	paddingBottom:10,
      	position:'absolute',
    	right:0,
    	left:0,
    	bottom:0,
        backgroundColor: '#' + colors[1],
    	color:'#' + colors[4] 
    },
    button:{
    	width: 100
    }
});
export default function App() {
	const ButtonColor = {
		Play:'#' + colors[4],
		Dares:'#' + colors[4],
		Users:'#' + colors[4],
		Tags:'#' + colors[4],
	}
	const PageDoms= {
		Play:<Text> this is a play</Text>,
		Dares:<Text> this is a Dares</Text>,
		Tags:<TagsPage/>, 
		Users:<UsersPage/>
	}
	const [page, setPage] = useState('Users');

	const PageDom = PageDoms[page];
	console.log(page)
	ButtonColor[page] = '#' + colors[2] 
  	
  	return (
		<View style={styles.container}>
			<Header title={page}></Header>
			 <ScrollView style={styles.contentContainer}> 
        		<View style={styles.inputWrapper}>
        			{PageDom}
        		</View>
    		</ScrollView>

    		<View style={styles.footer}>
    			<FontAwesome5 name="dice" onPress={() => {setPage('Play')}}  size={24} color={ButtonColor['Play']} />
    			<FontAwesome5 name="th-list" onPress={() => setPage('Dares')} size={24} color={ButtonColor['Dares']}/>
    			<FontAwesome5 name="users-cog" onPress={() => setPage('Users')} size={24} color={ButtonColor['Users']} />
    			<FontAwesome5 name="tags" onStartShouldSetResponder={() => setPage('Tags')} size={24} color={ButtonColor['Tags']} />
    		</View>
		</View>
  	);
}
